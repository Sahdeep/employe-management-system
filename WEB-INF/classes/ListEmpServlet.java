import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.*;
import java.util.*;
@WebServlet("/list")
public class ListEmpServlet extends HttpServlet 
{	
	public void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException,IOException
	{
		List<Employee> result=ModelComponent.getAllEmployeesInfo();
		String target=null;
		if(result.size() !=0)
		{
			req.setAttribute("result",result);
			target="results.jsp";
		}
		else
		{
			req.setAttribute("MSG","Employee Information is not available!!!!");
			target="home.jsp";
		}
		RequestDispatcher rd=req.getRequestDispatcher(target);
		rd.forward(req,resp);
		
	}
	
}
