public class Employee
{
	private int eno;
	private String ename;
	private double esal;
	private String eaddr;
	public void setEno(int eno)
	{
		this.eno=eno;
	}
	public int getEno()
	{
		return eno;
	}
	public void setEname(String ename)
	{
		this.ename=ename;
	}
	public String getEname()
	{
		return ename;
	}
	public void setEsal(double esal)
	{
		this.esal=esal;
	}
	public double getEsal()
	{
		return esal;
	}
	public void setEaddr(String eaddr)
	{
		this.eaddr=eaddr;
	}
	public String getEaddr()
	{
		return eaddr;
	}
	

}
