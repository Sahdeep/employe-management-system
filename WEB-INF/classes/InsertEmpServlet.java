import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import javax.servlet.annotation.*;
@WebServlet("/insert")
public class InsertEmpServlet extends HttpServlet 
{	
	public void doGet(HttpServletRequest req,HttpServletResponse resp) throws ServletException,IOException
	{
		String enumber=req.getParameter("eno");
		int eno=Integer.parseInt(enumber);
		String ename=req.getParameter("ename");
		String esalary=req.getParameter("esal");
		double esal=Double.parseDouble(esalary);
		String eaddr=req.getParameter("eaddr");
		int result=ModelComponent.insertEmployee(eno,ename,esal,eaddr);
		String target=null;
		if(result !=0)
		{
			req.setAttribute("MSG",ename+" Info Inserted SuccessFully");
			target="home.jsp";
		}
		else
		{
			req.setAttribute("MSG","Employee Insertion Fails.Plz try again!!!!");
			target="insertemp.jsp";
		}
		RequestDispatcher rd=req.getRequestDispatcher(target);
		rd.forward(req,resp);
		
	}
	
}
